repo: https://gitlab.upt.ro/vlad.carnat/animal-farm-automatization

Animal Farm Automatization
This code is designed to automate tasks in an animal farm using an ESP8266 Wi-Fi module, Blynk, servo motors, an LCD, and other components. The code allows you to control the system remotely through the Blynk app.

Features
Automatic water and food feeding for animals.
Water and food level monitoring using sensors.
Gate control for managing access to water and food compartments.
Cleaning functionality for both water and food sides.
Real-time status updates on an LCD display.
Hardware Requirements
ESP8266 Wi-Fi module
Servo motors
I2C LCD module
Water level sensor
LEDs
Dependencies
ESP8266_Lib.h
BlynkSimpleShieldEsp8266.h
Servo.h
Wire.h
LiquidCrystal_I2C.h
SoftwareSerial.h
Blynk Configuration
Make sure you have the Blynk app installed on your device. Use the following Blynk template details:

Template ID: TMPL4dNZ1J4Zs
Template Name: Animal Farm Automatization
Authorization Token: kqdVqBJroLnMjknPzLTflamfwANxaIZi
Setup
Connect the required hardware components to your ESP8266 module as per the instructions.
Set your Wi-Fi credentials in the ssid and pass variables.
Adjust the ESP8266 baud rate if necessary (#define ESP8266_BAUD 38400).
Customize pin assignments and other parameters as needed.
Blynk Virtual Pins
V1: Add Water (trigger)
V2: Add Food (trigger)
V3: Clean Water-Side (trigger)
V4: Clean Food-Side (trigger)
Functions
startMotor_food(): Starts the food motor in the clockwise direction.
returnMotor_food(): Reverses the food motor's direction.
stopMotor_food(): Stops the food motor.
startMotor_water(): Starts the water motor in the clockwise direction.
returnMotor_water(): Reverses the water motor's direction.
stopMotor_water(): Stops the water motor.
Blynk Widget Functions
BLYNK_WRITE(V1): Handles the "Add Water" action. Monitors the water level and adds water until a certain threshold is reached.
BLYNK_WRITE(V3): Handles the "Clean Water-Side" action. Closes the water gate, starts the water motor, and cleans the water-side.
BLYNK_WRITE(V2): Handles the "Add Food" action. Moves the food servo to add food.
BLYNK_WRITE(V4): Handles the "Clean Food-Side" action. Closes the food gate, starts the food motor, and cleans the food-side.
Main Loop
The loop() function continuously runs the Blynk application to maintain communication with the Blynk server.

Remember to avoid using delay() function in your code as it can interfere with Blynk's functionality.

Please ensure you have the required hardware components and libraries installed before using this code. For any further assistance, refer to the documentation of the respective components or consult the developer.