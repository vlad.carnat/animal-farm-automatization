#define BLYNK_TEMPLATE_ID "TMPL4dNZ1J4Zs"
#define BLYNK_TEMPLATE_NAME "Animal Farm Automatization"
#define BLYNK_AUTH_TOKEN "kqdVqBJroLnMjknPzLTflamfwANxaIZi"

#define BLYNK_PRINT Serial

#include <ESP8266_Lib.h> //Library for the ESP8266 Wi-Fi module 
#include <BlynkSimpleShieldEsp8266.h> //Library for Blynk
#include <Servo.h> //Library for the servo motors
#include <Wire.h> //Library for the I2C protocol
#include <LiquidCrystal_I2C.h>  //Library for the LCD
#include <SoftwareSerial.h> 


SoftwareSerial EspSerial(2, 3); // RX, TX
// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);
WidgetLCD lcd1(V0);

// Your WiFi credentials.

char ssid[] = "UPC38EF34F";
char pass[] = "Qw12121212";



// Your ESP8266 baud rate:
#define ESP8266_BAUD 38400

ESP8266 wifi(&EspSerial);

int WaterSensorPin=A0;

int motor_food_CK=6;
int motor_food_CCK=7;
int motor_food_speed=5;
int motor_water_CK=11;
int motor_water_CCK=12;
Servo add_foodServo;
Servo gate_water;
Servo gate_food;
int add_waterLED=8;
int add_foodLED=9;
int cleanLED=10;

void startMotor_food(){
  digitalWrite(motor_food_CK,HIGH);
  digitalWrite(motor_food_CCK,LOW);
  
void returnMotor_food(){
  digitalWrite(motor_food_CK,LOW);
  digitalWrite(motor_food_CCK,HIGH);
  
void stopMotor_food(){
  digitalWrite(motor_food_CK,LOW);
  digitalWrite(motor_food_CCK,LOW);
}

void startMotor_water(){
  digitalWrite(motor_water_CK,HIGH);
  digitalWrite(motor_water_CCK,LOW);
 
}
void returnMotor_water(){
  digitalWrite(motor_water_CK,LOW);
  digitalWrite(motor_water_CCK,HIGH);
  
}
void stopMotor_water(){
  digitalWrite(motor_water_CK,LOW);
  digitalWrite(motor_water_CCK,LOW);
}
BLYNK_WRITE(V1) //add water
{
   int pinValue = param.asInt(); 
  if(pinValue==1)
  {
  
  digitalWrite(add_waterLED, HIGH);
  lcd.clear();
  lcd1.clear();
  lcd.setCursor(0, 0);
  lcd.print("Process Started: ");
  lcd.setCursor(0, 1);
  lcd.print("Add Water");
  lcd1.print(0,0,"Process Started");
  lcd1.print(0,1,"Add Water");
  int WaterLevel=analogRead(WaterSensorPin);
  while(WaterLevel<600)
  {
    WaterLevel=analogRead(WaterSensorPin);
      digitalWrite(4,HIGH);
  }
  digitalWrite(4,LOW);
  
  digitalWrite(add_waterLED, LOW); 
   digitalWrite(5, LOW); 
  lcd.clear();
  lcd1.clear();
  lcd1.print(0, 0, "System Ready");
}
}

BLYNK_WRITE(V3) //clean water-side
{
   int pinValue = param.asInt(); 
  if(pinValue==1)
  {
  digitalWrite(cleanLED, HIGH);
  lcd.clear();
  lcd1.clear();
  lcd.setCursor(0, 0);
  lcd.print("Process Started: ");
  lcd1.print(0,0,"Cleaning");
  lcd.setCursor(0, 1);
  lcd.print("Closing Gate");
  lcd1.print(0,1,"Closing Gate");

  for(int i=90; i<=180;i++)
  {
  gate_water.write(i);
  delay(100);
  }

  startMotor_water();
  delay(1000);
  stopMotor_water();
  returnMotor_water();
  delay(1000);
  stopMotor_water();
  
  for(int i=180; i>=90;i--)
  {
  gate_water.write(i);
  delay(100);
  }
  digitalWrite(cleanLED, LOW); 
  lcd.clear();
  lcd1.clear();
  lcd1.print(0, 0, "System Ready");
}
}


BLYNK_WRITE(V2) //add food
{
   int pinValue = param.asInt(); 
  if(pinValue==1)
  {
  digitalWrite(add_foodLED, HIGH);
  lcd.clear();
  lcd1.clear();
  lcd.setCursor(0, 0);
  lcd.print("Process Started: ");
  lcd1.print(0,0,"Process Started");
  //Blynk.virtualWrite(V5,"Process Started: ");
  lcd.setCursor(0, 1);
  lcd.print("Add Food");
  lcd1.print(0,1,"Add Food");
  //Blynk.virtualWrite(V6,"Add Food");
  for(int i=90; i<=180;i++)
  {
  add_foodServo.write(i);
  delay(10);
  }
  
  
  for(int i=180; i>=90;i--)
  {
  add_foodServo.write(i);
  delay(10);
  }
  digitalWrite(add_foodLED, LOW); 
 
  lcd.clear();
  lcd1.clear();
  lcd1.print(0, 0, "System Ready");
}
}


BLYNK_WRITE(V4) //clean food-side
{
  int pinValue = param.asInt(); 
  if(pinValue==1)
  {
  digitalWrite(cleanLED, HIGH);
  lcd.clear();
  lcd1.clear();
  lcd.setCursor(0, 0);
  lcd.print("Process Started: ");
  //lcd1.print(0,0,"Started");
 // Blynk.virtualWrite(V5,"Process Started: ");
  lcd.setCursor(0, 1);
  lcd.print("Closing Gate");
  lcd1.print(0,1,"Closing Gate");
 // Blynk.virtualWrite(V6,"Closing Gate ");
  for(int i=90; i<=180;i++)
  {
  gate_food.write(i);
  delay(100);
  }

  
  startMotor_food();
  delay(1000);
  stopMotor_food();
  returnMotor_food();
  delay(1000);
  stopMotor_food();
  

  for(int i=180; i>=90;i--)
  {
  gate_food.write(i);
  delay(100);
  }
  digitalWrite(cleanLED, LOW); 
  lcd.clear();
  lcd1.clear();
  lcd1.print(0, 0, "System Ready");
 // Blynk.virtualWrite(V5,"                ");
 
}
}

void setup()
{
  lcd.begin();
	lcd.backlight();

  // Debug console
  Serial.begin(115200);
  
  add_foodServo.attach(A1);
  gate_water.attach(A2);
  gate_food.attach(A3);
  
  EspSerial.begin(ESP8266_BAUD);
  delay(10);
  pinMode(add_waterLED, OUTPUT);
  pinMode(add_foodLED, OUTPUT);
  pinMode(cleanLED, OUTPUT);
  pinMode(motor_food_CK, OUTPUT);
  pinMode(motor_food_CCK, OUTPUT);
  pinMode(motor_food_speed, OUTPUT);
  pinMode(4,OUTPUT);

  Blynk.begin(BLYNK_AUTH_TOKEN, wifi, ssid, pass, "blynk.cloud", 80);

}

void loop()
{
  
  Blynk.run();
  
  // You can inject your own code or combine it with other sketches.
  // Check other examples on how to communicate with Blynk. Remember
  // to avoid delay() function!
}